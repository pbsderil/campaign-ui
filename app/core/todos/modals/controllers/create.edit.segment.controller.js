(function () {
    'use strict';

    angular.module('app.todos.list')
           .controller('createEditSegmentController', createEditSegmentController);

    createEditSegmentController.$inject = ['SegmentService','ToasterService','$uibModalInstance'];

    function createEditSegmentController(SegmentService,ToasterService,$uibModalInstance){
      var ctrl = this;
      ctrl.entityTypeList = [];
      ctrl.selectedEntityType = {};
      ctrl.segmentStatus = "1";
      ctrl.segmentAction = "ADD";
      ctrl.segmentName = "";
      ctrl.segmentQuery = "";

      SegmentService.fetchEntityTypes().then(function(response){
        ctrl.entityTypeList  = response.data;
        ctrl.selectedEntityType = ctrl.entityTypeList[0];
      });


      ctrl.saveSegment = function(){

        var data = {
          name : ctrl.segmentName,
          query : ctrl.segmentQuery,
          isActive : (ctrl.segmentStatus == 1 ? true:false),
          actionType : ctrl.segmentAction,
          entityTypeDetailId : ctrl.selectedEntityType.id
        };

        SegmentService.createSegment({},data).then(function(response){
          if(response.success){
              ToasterService.success("Segment created");
              ctrl.ok();
          }else{
              ToasterService.error(response.error);
          }
          
        });
      }

      ctrl.ok = function () {
        $uibModalInstance.close('');
      };

      ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };

    }
})();