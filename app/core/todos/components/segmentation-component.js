(function () {
    'use strict';

    let segmentationComponent = {
        templateUrl: 'core/todos/templates/segmentation-component-view.html',
        bindings: {
          hero: '='
        }
    }

    angular.module('app.todos.list')
           .component('segmentationComponent', segmentationComponent);
})();
