(function () {
    'use strict';

    let campaignTypeComponent = {
        templateUrl: 'core/todos/templates/campaign-type-component-view.html',
        bindings: {
          hero: '='
        }
    }

    angular.module('app.todos.list')
           .component('campaignTypeComponent', campaignTypeComponent);
})();
