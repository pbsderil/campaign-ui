(function () {
    'use strict';

    let customSegmentationComponent = {
        templateUrl: 'core/todos/templates/custom-segmentation-component-view.html',
        bindings: {
          hero: '='
        },
        controller : customSegmentationComponentController ,
        controllerAs  : "ctrl"
    }

    customSegmentationComponentController.$inject = ['SegmentService','ToasterService','$uibModal'];

    function customSegmentationComponentController(SegmentService,ToasterService,$uibModal){
    	var ctrl = this;
    	ctrl.entityTypeList = [];
    	ctrl.selectedEntityType = {};
    	ctrl.segmentStatus = "1";
    	ctrl.segmentAction = "ADD";
    	ctrl.segmentName = "";
    	ctrl.segmentQuery = "";
    	ctrl.segmentList = [];
        ctrl.gridOptions ={};

    	ctrl.openCreateModal = function(){

    		var modalInstance = $uibModal.open({
		      animation: true,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'core/todos/modals/templates/create.edit.segment.modal.html',
		      controller: 'createEditSegmentController',
		      controllerAs : 'ctrl',
		      size: 'lg',
		      resolve: {
		        items: function () {
		          return "";
		        }
		      }
		    });

		    modalInstance.result.then(function (selectedItem) {
		    }, function () {
		    });

    	}

    	ctrl.getSegmentList = function(){

    		SegmentService.fetchSegmentList({"offset":0,"limit":400},{"purposeId":3,"purpose":"Segments"}).then(function(response){
		        ctrl.segmentList  = response.data[0].tagDtoListOld;
                ctrl.gridOptions = SegmentService.transformSegmentData(ctrl.segmentList);
	        });
    	}
    	ctrl.getSegmentList();
    }

    angular.module('app.todos.list')
           .component('customSegmentationComponent', customSegmentationComponent);
})();