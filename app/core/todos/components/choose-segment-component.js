(function () {
    'use strict';

    let chooseSegmentComponent = {
        templateUrl: 'core/todos/templates/choose-segment-component-view.html',
        bindings: {
          hero: '='
        }
    }

    angular.module('app.todos.list')
           .component('chooseSegmentComponent', chooseSegmentComponent);
})();
