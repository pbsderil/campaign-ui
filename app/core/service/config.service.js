(function () {
    'use strict';

    angular
        .module('app')
        .factory('ConfigService', ConfigService);

    ConfigService.$inject = ['$http'];

    function ConfigService($http, configService) {

        function getCampaignManagerBaseURL(){
            return "http://95.216.39.188:8085/campaignManager";
        }

        return {
            getCampaignManagerBaseURL:getCampaignManagerBaseURL
        };
    }
})();