(function () {
    'use strict';

    angular
        .module('app')
        .factory('ToasterService', ToasterService);

    ToasterService.$inject = ['$http', 'ConfigService','toaster'];

    function ToasterService($http, ConfigService, toaster) {


        function success(message){
            toaster.pop('success', "Success", message , 3000)
        }

        function error(message){
            toaster.pop('error', "Error", message, 3000)
        }

        return {
            success: success,
            error : error
        };
    }
})();