(function () {
    'use strict';

    angular
        .module('app.todos.list')
        .factory('SegmentService', SegmentService);

    SegmentService.$inject = ['$http', 'ConfigService'];

    function SegmentService($http, ConfigService) {

        var handleRemoteError = function(data) {
            return data.data;
        };

        var handleSuccess = function(data) {
            return data.data;
        };

        function fetchEntityTypes(params){
            return $http.get(ConfigService.getCampaignManagerBaseURL()+'/hercules/entityTypeDetail?'+$.param(params)).then(handleSuccess, handleRemoteError);
        }

        function createSegment(params,data){
            return $http.post(ConfigService.getCampaignManagerBaseURL()+'/hercules/createOrEdit',data).then(handleSuccess, handleRemoteError);
        }

        function fetchSegmentList(params,data){
            return $http.post(ConfigService.getCampaignManagerBaseURL()+'/hercules/tagGroup/fetchTagGroupsByFilter?'+$.param(params),data).then(handleSuccess, handleRemoteError);
        }

        function transformSegmentData(data){
            var obj = {};
            obj.columnDefs= [
                        {name: 'action', displayName:'Action', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><a href="{{row.getProperty(age)}}">Visible text</a></div>'}
                    ];
            angular.forEach(data,function(segment){
            });
            obj.data = data; 
            return data;
        }

        return {
            fetchEntityTypes: fetchEntityTypes,
            createSegment : createSegment,
            fetchSegmentList : fetchSegmentList,
            transformSegmentData : transformSegmentData
        };
    }
})();